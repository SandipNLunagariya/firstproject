variable "instance_name" {

	description = "Google Compute Name"
	type = "string"

	default = "first-terraform-instance"

}

variable "instance_type" {

	description = "Google Compute Type"
	type = "string"

	default = "n1-standard-1"

}

variable "image_name" {

	description = "Google Compute Image/OS Type"
	type = "string"

	default = "debian-cloud/debian-9"

}