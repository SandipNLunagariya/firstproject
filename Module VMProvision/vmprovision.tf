resource "google_compute_instance" "vm_instance" {
  name         = "${var.instance_name}"
  machine_type = "${var.instance_type}"
  metadata_startup_script = "apt-get update && app-get install -y apache2.0 && apt-get update && app-get install -y nginx"


  boot_disk {
    initialize_params {
      image = "${var.image_name}"
    }
  }


network_interface {
    network = "default"
    access_config {
    }
 }

}






