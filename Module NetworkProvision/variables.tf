variable "network_name" {

	description = "Google Network Name"
	type = "string"

	default = "first-terraform-network"

}


variable "cidr_vpc" {
  description = "CIDR block for the VPC"
  default = "10.1.0.0/16"
}

variable "cidr_subnet" {
  description = "CIDR block for the subnet"
  default = "10.1.0.0/24"
}

variable "availability_zone" {
  description = "availability zone to create subnet"
  default = "us-central-1c"
}

# variable "public_key_path" {
#   description = "Public key path"
#   default = "~/.ssh/id_rsa.pub"
# }

variable "environment_tag" {
  description = "Environment tag"
  default = "Dev"
}
