resource "google_compute_network" "vpc_network" {
 
  name                    = "first-terraform-network"
  auto_create_subnetworks = "true"

}